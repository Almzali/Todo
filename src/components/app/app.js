import React from 'react';
import AppHeader from '../app-header'
import SearhPanel from '../search-panel'
import ItemStatusFilter from '../item-status-filter'
import TodoList from '../todo-list'

import './app.css'

class App extends React.Component  {

  state ={
    todos: [
      { id: 1, label: 'Kyrgystan', important: true,done: false},
      { id: 2, label: 'Kazakhstan', important: false,done: false},
      { id: 3, label: 'Uzbekistan', important: false,done: false},
      { id: 4, label: 'China', important: false,done: false},
      { id: 5, label: 'USA', important: false,done: false},
      { id: 6, label: 'Russia', important: false,done: false},
      { id: 7, label: 'Italy', important: false,done: false}
    ]
  }

  

  toggleImportant = (id) => {
    
  this.setState((oldState) =>{
    const idx = oldState.todos.findIndex((item) =>item.id ==id)
    const element_old = oldState.todos[idx]
    const element_new = {...element_old, important: !element_old.important}

    const prev = oldState.todos.slice(0, idx)
    const next = oldState.todos.slice(idx + 1)

    const new_todos = [...prev, element_new, ...next]

      return { todos: new_todos}
    })
  }
  render() {


          return (
            <div className="todo-app">
              <AppHeader toDo = {1} done = {7} /> 

              <div className="top-panel d-flex">
              <SearhPanel />   
              <ItemStatusFilter />
              </div>
              <TodoList toggleImportant={this.toggleImportant} todos={this.state.todos} />
            </div>
          );
        };
  };

  export default App;